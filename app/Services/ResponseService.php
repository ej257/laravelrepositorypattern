<?php

namespace App\Services;

class ResponseService
{
    /**
     * Return response with token.
     *
     * @param string $token
     * @param int $code
     * @param array  $additionalData
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseWithToken(string $token, int $code = 200, array $additionalData = []): \Illuminate\Http\JsonResponse
    {
        $standardData = ['token' => $token,
            'token_type'         => 'bearer',
            'expires_in'         => auth()->factory()->getTTL() * 60,
        ];

        $data = array_merge($standardData, $additionalData);

        return $this->responseJson($data, $code);
    }

    /**
     * Return unauthorized response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseUnauthorized(): \Illuminate\Http\JsonResponse
    {
        return $this->responseError([
            'code'    => 401,
            'message' => __('auth.unauthorized'),
        ]);
    }

    /**
     * Return custom error response.
     *
     * @param array $data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseError(array $data): \Illuminate\Http\JsonResponse
    {
        $data['code'] = $data['code'] ?? 500;
        $data['message'] = $data['message'] ?? __('messages.server_error_500');

        return response()->json([
            'error' => [
                $data,
            ],
        ], $data['code']);
    }

    /**
     * Return json response.
     *
     * @param array $data
     * @param int   $code
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseJson(array $data, int $code = 200): \Illuminate\Http\JsonResponse
    {
        return response()->json($data, $code);
    }
}
