<?php

namespace App\Services;

use App\Contracts\UserInterface;
use App\Models\User;
use Illuminate\Http\Request;

class AuthService
{
    /**
     * @var ResponseService
     */
    private $responseService;

    /**
     * @var UserInterface
     */
    private $userRepository;

    /**
     * UserService constructor.
     *
     * @param ResponseService $responseService
     * @param UserInterface   $userRepository
     */
    public function __construct(ResponseService $responseService, UserInterface $userRepository)
    {
        $this->responseService = $responseService;
        $this->userRepository = $userRepository;
    }

    /**
     * Register user.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request): \Illuminate\Http\JsonResponse
    {
        $data = $request->only([
            'name', 'email', 'password',
        ]);

        $user = User::create($data);

        $token = auth()->login($user);

        return $this->responseService->responseWithToken($token, 201, $user->getToRest());
    }

    /**
     * Login user.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request): \Illuminate\Http\JsonResponse
    {
        $data = $request->only(['email', 'password']);

        $token = auth()->attempt($data);
        if (!$token) {
            return $this->responseService->responseError([
                'code'    => 422,
                'message' => __('auth.failed'),
            ]);
        }

        $userToRest = $this->userRepository->toRest(auth()->user()->id);

        return $this->responseService->responseJson($userToRest);
    }
}
