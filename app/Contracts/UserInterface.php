<?php

namespace App\Contracts;

interface UserInterface
{
    /**
     * Get all models.
     */
    public function all();

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function toRest(int $id);
}
