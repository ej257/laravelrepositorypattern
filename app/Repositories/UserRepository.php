<?php

namespace App\Repositories;

use App\Contracts\UserInterface;
use App\Models\User;
use App\Services\ResponseService;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserRepository implements UserInterface
{
    /**
     * @var ResponseService
     */
    private $responseService;

    /**
     * UserRepository constructor.
     *
     * @param ResponseService $responseService
     */
    public function __construct(ResponseService $responseService)
    {
        $this->responseService = $responseService;
    }

    /**
     * Get all users.
     */
    public function all()
    {
        try {
            return User::all()->toArray();
        } catch (\Exception $exception) {
            return $this->responseService->responseError([
                'code'    => 500,
                'message' => __('messages.server_error_user_all'),
            ]);
        }
    }

    /**
     * Transfer user to rest.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function toRest(int $id)
    {
        try {
            $user = User::findOrFail($id);

            return $user->getToRest();
        } catch (ModelNotFoundException $exception) {
            return $this->responseService->responseError([
                'code'    => 404,
                'message' => __('messages.invalid_user'),
            ]);
        } catch (\Exception $exception) {
            return $this->responseService->responseError([
                'code'    => 500,
                'message' => __('messages.server_error_user_get'),
            ]);
        }
    }
}
