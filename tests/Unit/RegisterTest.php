<?php

namespace Tests\Unit;

use Tests\TestCase;
use Faker\Generator as Faker;

class RegisterTest extends TestCase
{
    /**
     * @var Faker
     */
    private $faker;

    /**
     * Dependency injection
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->faker = $this->app->make(Faker::class);
    }

    /**
     * Test if user can register
     *
     * @return mixed
     */
    public function testUserCanRegister()
    {
        $email = $this->faker->unique()->safeEmail;
        $payload = [
            'name' => $this->faker->name,
            'email' => $email,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
        ];

        $response = $this->post(route('api.register'), $payload);

        $response->assertStatus(201);
        $this->assertDatabaseHas('users', [
            'email' => $email,
        ]);
    }
}
