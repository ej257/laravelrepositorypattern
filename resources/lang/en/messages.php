<?php

return [
    'invalid_user'          => 'User not found',
    'server_error_user_get' => 'Error while getting user data',
    'server_error_user_all' => 'Error while getting all users',
    'server_error_500'      => 'Something went wrong, please try again later',
];
